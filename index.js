const Sequelize = require('sequelize');
let express = require('express');
let app = express();
app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));


var sequelize = new Sequelize('rubrica', 'rubrica', 'rubrica', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },
});

let Contact = sequelize.define('contact', {
    name: Sequelize.STRING(42)
});

let Telephone = sequelize.define('telephone', {
    telephone: { type: Sequelize.STRING(11) },
    type: Sequelize.ENUM('casa', 'cellulare', 'lavoro', 'fax')
});

Contact.hasMany(Telephone, { onDelete: 'CASCADE' });
Telephone.belongsTo(Contact);

sequelize.sync({ force: true }).then(() => {
    /*Contact.create({
        name: 'Prova1',
        telephones: [
            { telephone: '111111111', type: 'cellulare' }
        ]
    }, {
        include: [Telephone]
    });
    Contact.create({
        name: 'Prova2',
        telephones: [
            { telephone: '222222222', type: 'fax' },
            { telephone: '222222221', type: 'cellulare' }
        ]
    }, {
        include: [Telephone]
    });

    Contact.create({
        name: 'Prova3',
        telephones: [
            { telephone: '333333333', type: 'cellulare' },
            { telephone: '333333331', type: 'lavoro' },
        ]
    }, {
        include: [Telephone]
    });*/

    app.get('/', (req, res) => {
        Contact.findAll({ include: [Telephone] }).then((risultato) => {
            console.log(risultato)
            console.log('get')
            res.render('index', { lista: risultato });
        });

    });

    app.post('/', (req, res) => {
        console.log(req.body);
        let telefoni = [];
        telefoni.push({ telephone: req.body.cell, type: 'cellulare' });
        telefoni.push({ telephone: req.body.home, type: 'casa' });
        telefoni.push({ telephone: req.body.work, type: 'lavoro' });
        telefoni.push({ telephone: req.body.facs, type: 'fax' });

        Contact.create({
            name: req.body.name,
            telephones: telefoni
        }, {
            include: [Telephone]
        }).then(()=>{
            res.send('ok');
        });
        
    });
    app.delete('/', (req, res) => {
        console.log(req.query.id)
        console.log('del')

        Contact.findByPk(req.query.id).then((contatto) => {
            return contatto.destroy();
        }).then(() => {
            res.send('ok')
        });
    });

    app.get('/singolo', (req, res) => {
        Contact.findByPk(req.query.id, {
            include: [Telephone]
        }).then((contatto) => {
            res.send(contatto);
        })
    });

    app.put('/', (req, res) => {
        console.log(req.body);
        let telefoni = [];
        telefoni.push({ telephone: req.body.cell, type: 'cellulare' });
        telefoni.push({ telephone: req.body.home, type: 'casa' });
        telefoni.push({ telephone: req.body.work, type: 'lavoro' });
        telefoni.push({ telephone: req.body.facs, type: 'fax' });
        
      console.log("array prima del update",telefoni)
        //aggiungi l'aggiornamneto di telefoni
   /*   Contact.update({
            name: req.body.name,
            telephones:telefoni
        }, {
            where: { id: req.body.id },
            returning:true,
            plain:true
        }).then(() => {
            res.send("va");
        });*/
        Contact.findByPk(req.body.id).then((risultato)=>{
            console.log("entrato nella promise",risultato);
            
            return risultato.update({
                name:req.body.name,
            })
        }).then(()=>{
           telefoni.forEach((elem) => {
                Telephone.findAll({ where: { contactId: req.body.id, type: elem.type } }).then((ris) => {
                    console.log("#############################", ris);
                    ris[0].update({ telephone: elem.telephone }).then(()=>{
                        if(elem.type=="fax"){
                            res.send("va");
                        }
                    });
                });
                
            });
            
        });


    });

    app.listen(8080, () => { console.log('Server avviato') });
});
