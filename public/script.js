//#F9036E rosa
//#0087D3 blu
var idupdatecorrente;
$(document).ready(function () {
    console.log("ready")
    $('#aggiungi').click(() => {
        $('#popupaggiungi').fadeIn();
    });
    $('#salvapopup').click(() => {
        let nome = $('#nome').val();
        let cellulare = $('#cellulare').val();
        let casa = $('#casa').val();
        let lavoro = $('#lavoro').val();
        let fax = $('#fax').val();
        $.ajax({
            url: '/',
            type: 'POST',
            data: {
                name: nome,
                cell: cellulare,
                home: casa,
                work: lavoro,
                facs: fax
            },
            success: () => {
                console.log("va")
                let nome = $('#nome').val("");
                let cellulare = $('#cellulare').val("");
                let casa = $('#casa').val("");
                let lavoro = $('#lavoro').val("");
                let fax = $('#fax').val("");
                $('#popupaggiungi').hide();
                window.location.replace("/");
            },
            error: () => {
                console.log("non va")
                let nome = $('#nome').val("");
                let cellulare = $('#cellulare').val("");
                let casa = $('#casa').val("");
                let lavoro = $('#lavoro').val("");
                let fax = $('#fax').val("");
                $('#popupaggiungi').fadeOut();
            },
        });
    });
    $('#annullapopupaggiungi').click(() => {
        $('#popupaggiungi').fadeOut();
    });
    $('.delete').click((e) => {
        let id = e.target.id.substring(6)
        console.log(id)
        $.ajax({
            url: '/' + '?' + $.param({ "id": id }),
            type: 'DELETE',
            success: () => {
                console.log("va")
                window.location.replace("/");
            },
            error: () => { console.log("non va") },
        });
    });
    $('.edita').click((e) => {
        console.log(e);
        let id = e.currentTarget.id.substring(5);
        console.log(id);
        $.ajax({
            url: '/singolo' + '?' + $.param({ "id": id }),
            type: 'GET',
            success: (response) => {
                console.log("va",response)
                let tels=response.telephones;
                $('#nomeupdate').val(response.name);

                let cel=tels.find(t=>t.type ==='cellulare');
                let cas=tels.find(t=>t.type ==='casa');
                let lav = tels.find(t=>t.type ==='lavoro');
                let fa = tels.find(t=>t.type ==='fax');
                $('#cellulareupdate').val(cel!=undefined?cel.telephone:"");
                $('#casaupdate').val(cas!=undefined?cas.telephone:"");
                $('#lavoroupdate').val(lav!=undefined?lav.telephone:"");
                $('#faxupdate').val(fa!=undefined?fa.telephone:"");
                idupdatecorrente=id;
                $('#popupedita').fadeIn();
            },
            error: () => { console.log("non va") },
        });
    });
    $('#salvapopupupdate').click(()=>{
        let nome = $('#nomeupdate').val();
        let cellulare = $('#cellulareupdate').val();
        let casa = $('#casaupdate').val();
        let lavoro = $('#lavoroupdate').val();
        let fax = $('#faxupdate').val();
        $.ajax({
            url: '/',
            type: 'PUT',
            data: {
                id:parseInt(idupdatecorrente),
                name: nome,
                cell: cellulare,
                home: casa,
                work: lavoro,
                facs: fax
            },
            success: () => {
                console.log("va")
                window.location.replace("/");
            },
            error: () => {

            },
        });
    });
    $('#annullapopupupdate').click(() => {
        $('#popupedita').fadeOut();
    });
})



/*
$('#nomeupdate').val("");
                $('#cellulareupdate').val("");
                $('#casaupdate').val("");
                $('#lavoroupdate').val("");
                $('#faxupdate').val("");




*/